public class DoPayRequest {
    private String id;
    private From from;
    private To to;
    private String purpose;
    private Integer documentNumber;
    private Integer executionOrder;
    private Integer amount;
    private Meta meta;
    private String duedate;

    public DoPayRequest() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public From getFrom() {
        return from;
    }

    public void setFrom(From from) {
        this.from = from;
    }

    public To getTo() {
        return to;
    }

    public void setTo(To to) {
        this.to = to;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public Integer getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(Integer documentNumber) {
        this.documentNumber = documentNumber;
    }

    public Integer getExecutionOrder() {
        return executionOrder;
    }

    public void setExecutionOrder(Integer executionOrder) {
        this.executionOrder = executionOrder;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getDuedate() {
        return duedate;
    }

    public void setDuedate(String duedate) {
        this.duedate = duedate;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public String toString() {
        return "DoPayRequest{" +
                "id='" + id + '\'' +
                ", from=" + from +
                ", to=" + to +
                ", purpose='" + purpose + '\'' +
                ", documentNumber=" + documentNumber +
                ", executionOrder=" + executionOrder +
                ", amount=" + amount +
                ", meta=" + meta +
                ", duedate='" + duedate + '\'' +
                '}';
    }
}
