public class ErrorDetails {
    private String reason;

    public ErrorDetails() {
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "ErrorDetails{" +
                "reason='" + reason + '\'' +
                '}';
    }
}
